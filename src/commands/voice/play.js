'use strict';

const commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;
const _ = require('lodash');
const sounds = require('../../assets/sounds.json');

const soundPath = './src/assets/sounds/'

module.exports = class PlayCommand extends commando.Command {
  constructor(client) {
    super(client, {
      name: 'play',
      group: 'voice',
      memberName: 'play',
      description: 'Play sounds in the voice channel you\'re present it',
      details: oneLine`
        This causes the bot to join your voice channel, play the associated clip, then leave.
				Meme away!
			`,
      examples: ['play EZAF'],

      args: [
        {
          key: 'clip',
          label: 'clip',
          prompt: 'Which sound clip do you want to play?',
          type: 'string',
          infinite: false
        }
      ]
    });

    this.chatList = '';
    for (let i = 0; i < Object.keys(sounds).length; i++) {
      this.chatList = this.chatList + '\n' + Object.keys(sounds)[i];
    }
  }

  async run(msg, args) {
    let soundFile = null;

    try {
      let channel = msg.member.voiceChannel;

      for (let i = 0; i < Object.keys(sounds).length; i++) {
        if (args.clip.toLowerCase() === Object.keys(sounds)[i]) {
          soundFile = `${soundPath}${sounds[args.clip.toLowerCase()]}`;
        }
      }

      if (soundFile === null) {
        console.log(`${_.now()} ERROR: Sound List`);
        return msg.reply(`List of Sounds:\`\`\`${this.chatList}\`\`\``);
      }

      channel.join()
        .then(connection => {
          console.log(`Connected! Playing ${soundFile}`);
          const sound = connection.playFile(soundFile);
          sound.on('end', () => {
            channel.leave();
          });
        })
        .catch(console.error);

    } catch (err) {
      console.log(`${_.now()} Error with Play command ${err}`);
      return msg.reply(`An error occurred! The following must be true to use this command\n
        - Currently in a server's voice channel.\n
        - Command sent from a text channel of the same server.`)
    }
  }
};


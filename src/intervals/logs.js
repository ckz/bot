'use strict';

const Discord = require('discord.js');
const r = require('request-promise');
const _ = require('lodash');

class WCLog {
  constructor(config, client) {
    this.client = client;
    this.request = `${config.wclOpts.urlBase}/${config.wclOpts.guild}/${config.wclOpts.realm}/${config.wclOpts.region}?api_key=${config.wclOpts.apiKey}`;
    this.options = {
      uri: this.request,
      json: true
    };

    this.guildID = config.raid_guild_id;
    this.channelID = config.raid_channel_id;

    this.logDelay = 1000 * 60 * 5; // Interval delay set to 5 minutes

    this.checkLog();

    this.createInterval();
  }

  checkLog() {
    r(this.options)
      .then(data => {
        let recent = data[0];
        if (recent.id !== this.lastLog && recent.id !== undefined && this.lastLog !== undefined) {
          // Post Embed to Channel
          console.log(`${_.now()} Auto-posting latest log: ${recent.id}`);

          //Have to set these here for now, as not available immediately at bot start
          let guild = this.client.guilds.get(this.guildID);
          let channel = guild.channels.get(this.channelID);

          let embed = new Discord.RichEmbed();
          embed
            .setTitle(`${recent.title} uploaded by ${recent.owner}`)
            .setDescription(`https://www.warcraftlogs.com/reports/${recent.id}`)
            .setThumbnail('https://www.warcraftlogs.com/img/common/warcraft-logo.png')
            .setColor([255, 128, 0]) //LEGENDARY!
            .setFooter('New WarcraftLogs Upload')
            .setTimestamp();
          channel.sendEmbed(embed);

          this.lastLog = recent.id; // Set the new latest log
        } else if (this.lastLog === undefined) {
          // First check at startup
          this.lastLog = recent.id;
          console.log(`${_.now()} Current latest log is: ${this.lastLog}`);
        } else {
          console.log(`${_.now()} No new log detected, current last log is ${this.lastLog}`);
        }
      })
      .catch(err => {
        console.log(`${_.now()} Error with auto-log feature: ${err}`);
      });
  }

  createInterval() {
    let self = this;
    setInterval(function () {
      self.checkLog();
    }, self.logDelay);
    console.log(`${_.now()} Raid Webhook Interval established for ${self.request}`)
  }
}

module.exports = WCLog;

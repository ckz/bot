'use strict';

class Config {
  constructor() {
    this.wclOpts = {
      apiKey: process.env.WCL_API_KEY,
      guild: process.env.WCL_GUILD,
      realm: process.env.WCL_REALM,
      region: process.env.WCL_REGION || 'US',
      urlBase: 'https://www.warcraftlogs.com:443/v1/reports/guild'
    };


    this.discord_token = process.env.DISCORD_TOKEN;
    this.owner_id = process.env.OWNER_ID;
    this.prefix = process.env.PREFIX;
    this.raid_guild_id = process.env.RAID_GUILD_ID;
    this.raid_channel_id = process.env.RAID_CHANNEL_ID;

    for (let config in this) {
      if (this.hasOwnProperty(config) && this[config] === undefined) {
        // Exit code 9: 
        // Invalid Argument - Either an unknown option was specified, or an option requiring a value was provided without a value.
        // Close enough?
        console.log(`Missing required environment variable ${config.toUpperCase()}, exitting...`);
        process.exit(9)
      }
    }
  }
}

module.exports = Config;
